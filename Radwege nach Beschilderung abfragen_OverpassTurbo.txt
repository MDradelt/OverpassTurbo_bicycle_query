/*
Anzeige Radwege in Magdeburg nach Kriterium Beschilderung
*/

[out:json][timeout:25];

area(3600062481)->.searchArea; // fetch area “Magdeburg” to search in

( // Suchkriterien
  way["traffic_sign"~"DE:237"](area.searchArea); 	//Radweg
  way[~"traffic_sign"~"DE:237"](area.searchArea); 
  way["traffic_sign"~"DE:240"](area.searchArea);	//gemeinsamer Fuß-/Radweg
  way[~"traffic_sign"~"DE:240"](area.searchArea);
  way["traffic_sign"~"DE:241"](area.searchArea);
  way[~"traffic_sign"~"DE:241"](area.searchArea);	//getrennter Fuß- Radweg
  //way~["traffic_sign"~"1022"](area.searchArea);	//Zusatzschild "Radfahrer frei"
);

out body; // print results

>;

out skel qt;