## Zusatzcode um Beschilderung anzuzeigen


Dieser Teil erweitert die Suchkriterien um die Nodes mit Radwegbeschilderung. Die Markierungen/Symbole sind anklickbar und zeigen dann ihre OSM-Details.

```
  node[~"traffic_sign"~"237"](area.searchArea);
  node[~"traffic_sign"~"240"](area.searchArea);    
  node[~"traffic_sign"~"241"](area.searchArea);
  node[~"traffic_sign"~"244"](area.searchArea);
  node[~"traffic_sign"~"1022-10"](area.searchArea);
```

Dieser Teil erweitert die Darstellung um die Symbole. Es stehen (noch) nicht immer alle Kombinationen mit anderen Schildern oder Zusatzzeichen zur Verfügung. Es wird dann ein einfacher Kreis angezeigt.

```
node[traffic_sign=DE:237],
node[traffic_sign:forward=DE:237],
node[traffic_sign=DE:237,1000-22]
{icon-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Zeichen_237_-_Sonderweg_Radfahrer%2C_StVO_1992.svg/200px-Zeichen_237_-_Sonderweg_Radfahrer%2C_StVO_1992.svg.png');
  icon-width: 18; }

node[traffic_sign=DE:237,1012-31]
{icon-image: url('https://codeberg.org/MDradelt/OverpassTurbo_bicycle_query/raw/branch/main/img/Zeichen_237_1012-31_Radweg_Ende.png');
  icon-width: 18; icon-height: 28 }

node[traffic_sign=DE:239,1022-10],
node[traffic_sign=DE:239;1022-10],
node[traffic_sign=DE:239;DE:1022-10]
{icon-image: 
url('https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Zeichen_239_mit_Zusatzzeichen_10-2210.svg/200px-Zeichen_239_mit_Zusatzzeichen_10-2210.svg.png');
  icon-width: 18; icon-height: 28 }

node[traffic_sign=DE:240],
node[traffic_sign:forward=DE:240],
node[traffic_sign=DE:240;DE:1000-20],
node[traffic_sign=DE:283;DE:240]
{icon-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/0/08/Zeichen_240_-_Gemeinsamer_Fu%C3%9F-_und_Radweg%2C_StVO_1992.svg/200px-Zeichen_240_-_Gemeinsamer_Fu%C3%9F-_und_Radweg%2C_StVO_1992.svg.png');
  icon-width: 18; }

node[traffic_sign=DE:240,1000-31],
node[traffic_sign=DE:240;1000-31]
{icon-image: url('https://codeberg.org/MDradelt/OverpassTurbo_bicycle_query/raw/commit/0c08cd6004a3ee5d2814f75fb4cb7bc0aaa8aefc/img/Zeichen_240_1000-31.png');
 icon-width: 18;icon-height:30; }

node[traffic_sign=DE:240,1012-31]
{icon-image: url('https://codeberg.org/MDradelt/OverpassTurbo_bicycle_query/raw/branch/main/img/Zeichen_240_1012-31_gemeinsamer_Radweg_Ende.png');
  icon-width: 18; icon-height: 28 }


node[traffic_sign=DE:241],
node[traffic_sign:forward=DE:241],
node[traffic_sign=DE:241-30],
node[traffic_sign=DE:283-30;DE:241]
{icon-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Zeichen_241-30_-_getrennter_Rad-_und_Fu%C3%9Fweg%2C_StVO_1992.svg/200px-Zeichen_241-30_-_getrennter_Rad-_und_Fu%C3%9Fweg%2C_StVO_1992.svg.png');
  icon-width: 18;}

node[traffic_sign=DE:241,1000-31],
node[traffic_sign=DE:241-30,1000-31],
node[traffic_sign=DE:241-30;DE:1000-31]
{icon-image: url('https://codeberg.org/MDradelt/OverpassTurbo_bicycle_query/raw/branch/main/img/Zeichen_241_1000-31_getrennter%20Radweg%20Zweirichtung.png');
  icon-width: 18; icon-height: 28}

node[traffic_sign=DE:241-31]
{icon-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/2/26/Zeichen_241-31.svg/200px-Zeichen_241-31.svg.png');
  icon-width: 18; icon-height: 28}

node[traffic_sign=DE:242,1022-10]
{icon-image: url('https://codeberg.org/MDradelt/OverpassTurbo_bicycle_query/raw/branch/main/img/Zeichen_242_1022-10_Fu%c3%9fg%c3%a4ngerzone%20Radfahrer%20frei.png');
  icon-width: 18; icon-height: 28}

node[traffic_sign=DE:1022-10],
node[traffic_sign=DE:1022-10,1000-33]
{icon-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/Zusatzzeichen_1022-10_-_Radfahrer_frei%2C_StVO_1992.svg/320px-Zusatzzeichen_1022-10_-_Radfahrer_frei%2C_StVO_1992.svg.png');
  icon-width: 18; icon-height: 12}
```