## Abfrage ID

```
//Abfrage der Radwegeinfrastruktur für Magdeburg. 1. Abfrage Wegsegmentlängen und ID-Nr. -- 2. Abfrage ID-Nr. und Eigenschaften

[out:csv(key, len)]; //Längenermittlung ID und Wegsegmentlänge

area(3600062481)->.searchArea; // ganz Magdeburg
(
  way[~"traffic_sign"~"DE:237"](area.searchArea); //Radweg
  
  way["highway"!="footway"][~"traffic_sign"~"DE:240"](area.searchArea); //gemeinsamer Rad-Fußweg 
  way["highway"!="footway"][~"traffic_sign"~"DE:241"](area.searchArea); //getrennter Rad-Fußweg
  //Ausschluss des Fußweges für Bereiche, bei denen der cycleway und der footway parallel gezeichnet und beide mit Z240 bzw. 241 getaggt sind.

  way[~"traffic_sign"~"DE:244"](area.searchArea); //Fahrradstraße

  way["segregated"="yes"]["bicycle"~"yes|designated"](area.searchArea); //baulich angelegter nicht benutzungspflichtiger Radweg
  //way[~"bicycle"~"designated"](area.searchArea);
    
  way["highway"="cycleway"](area.searchArea); //baulich einzeln geführt (impl. keine Ben-Pflicht)
  //way["cycleway"="yes"](area.searchArea);
  //way["cycleway"="designated"](area.searchArea);

  way["bicycle_road"~"yes|designated"](area.searchArea); //Fahrradstraße
  
  way["bicycle"="lane"](area.searchArea); //Fahrradstreifen an Straßen
  way[~"cycleway"~"lane"](area.searchArea);
  way["bicycle:lanes"~"designated"](area.searchArea);
  way["bicycle:lanes"~"yes"](area.searchArea);
);

for (id())
  (  
  make stat key = _.val,
    len=sum(length());
    out; 
  );

```

## Abfrage Eigenschaften

```
[out:csv(::id, name, highway,oneway,"oneway:bicycle", traffic_sign,"traffic_sign:forward","traffic_sign:backward",segregated,surface,"surface:cycleway",width,"width:cycleway",bicycle,crossing,path,cycleway,"cycleway:left","cycleway:right","cycleway:both")];

// fetch area “Magdeburg” to search in
area(3600062481)->.searchArea;
(
  way[~"traffic_sign"~"DE:237"](area.searchArea);
  way["highway"!="footway"][~"traffic_sign"~"DE:240"](area.searchArea);
  way["highway"!="footway"][~"traffic_sign"~"DE:241"](area.searchArea);
  way[~"traffic_sign"~"DE:244"](area.searchArea);

  way["segregated"="yes"]["bicycle"~"yes|designated"](area.searchArea);
  //way[~"bicycle"~"designated"](area.searchArea);
    
  way["highway"="cycleway"](area.searchArea);//(impl. k. Ben-Pflicht)

  way["bicycle_road"~"yes|designated"](area.searchArea); //Fahrradstraße

  way["bicycle"="lane"](area.searchArea);
  way[~"cycleway"~"lane"](area.searchArea);
  way["bicycle:lanes"~"designated"](area.searchArea);
  way["bicycle:lanes"~"yes"](area.searchArea);
);

out; 
```

## Formel für zusätzliche Wegbenennungen

=SVERWEIS(D1600;'ID-Name'.$A$2:$'ID-Name'.$B$3000;2;0)